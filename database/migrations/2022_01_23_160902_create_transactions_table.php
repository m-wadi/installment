<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->string('unique_id')->nullable();
            $table->string('card')->nullable();
            $table->string('merchant_transaction_id');
            $table->double('amount');
            $table->text('shopper_result_url');
            $table->enum('status',['PAID', 'PENDING', 'DECLINED', 'ERROR','INITIALIZED','MIDDLEPAGE'])->default('PENDING');
            $table->foreignId('payment_setting_id')->constrained();
            $table->foreignId('merchant_id')->constrained();
            $table->text('result_details')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}

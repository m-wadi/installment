<?php

namespace Database\Seeders;

use App\Models\Bank;
use App\Models\Bin;
use App\Models\Plan;
use Illuminate\Database\Seeder;

class BanksAndBinsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // creating banks and bins
        $this->BAJbank();
        $this->ANBbank();
        $this->SABBbank();
        $this->NBDbank();
    }

    private function BAJbank()
    {
        $baj = Bank::create([
            'name' => 'Bank Al Jazera',
            'status' => 1,
            'image' => null, // upload later manually,
            'frequency' => 3,
            'country' => 'SA',
            'terms' => 'Cardholder should agree on the terms and conditions of the issuer bank before conducting the transaction. (attached)
            Find in the attachment transaction’s file.  <br>
             Applies to any credit card purchase transactions with amount over SAR 1500 and less the existing limit. <br>
            Applies on all of Bank AlJazira credit cards except the low limit cards.<br>
            A fee of SAR 50 will be charged to cardholder for each installment transaction (This fee is subject to 15% Value Added Tax (VAT).<br>
            Bank AlJazira has the full right to accept / reject any EPP transaction received by Hyper pay.',
            'email' => '[{"email":"AbAlOtaibi@BAJ.Com.SA"},{"email":"BAlAgeel@BAJ.Com.SA"},{"email":"FSAlMakadi@BAJ.Com.SA"}]',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $plan1 = Plan::create([
            'name' => '3 Months',
            'description' => 'Tenor of 3 months with a rate of 0% per month.',
            'bank_id' => $baj->id,
            'min_value' => 1500,
            'interest' => 0,
            'fees' => 50,
            'plan_code' => '3MonthsBAJ',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $plan2 = Plan::create([
            'name' => '6 Months',
            'description' => 'Tenor of 6 months with a rate of 1.5% pet month.',
            'bank_id' => $baj->id,
            'min_value' => 1500,
            'interest' => 1.5,
            'fees' => 50,
            'plan_code' => '6MonthsBAJ',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $plan3 = Plan::create([
            'name' => '9 Months',
            'description' => 'Tenor of 9 months with a rate of 1.2% pet month',
            'bank_id' => $baj->id,
            'min_value' => 1500,
            'interest' => 1.2,
            'fees' => 50,
            'plan_code' => '9MonthsBAJ',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $plan4 = Plan::create([
            'name' => '12 Months',
            'description' => 'Tenor of 12 months with a rate of 1% pet month.',
            'bank_id' => $baj->id,
            'min_value' => 1500,
            'interest' => 1,
            'fees' => 50,
            'plan_code' => '12MonthsBAJ',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $bajBins = ['428374', '411092', '411092', '428375', '440532', '473826', '473828', '473827', '414841', '489317', '524236', '546924', '511249'];
        foreach ($bajBins as $bin) {
            $bin = Bin::create([
                'bin' => $bin,
                'bank_id' => $baj->id,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
            $plan1->bins()->attach($bin->id);
            $plan2->bins()->attach($bin->id);
            $plan3->bins()->attach($bin->id);
            $plan4->bins()->attach($bin->id);
        }
    }

    private function ANBbank()
    {
        $anb = Bank::create([
            'name' => 'Arab National Bank',
            'status' => 1,
            'country' => 'SA',
            'image' => null, // upload later manually,
            'frequency' => 3,
            'terms' => '
                Customer must pay the purchase using ANB Credit Card. <br>
                The minimum purchase amount is SAR 1000 for transaction. <br>
                The available tenors is 3, 6, 9 and 12 months. <br>
                1% profit margin will be calculated on the total transaction amount.',
            'email' => '[{"email":"Account_Maintenance_Unit@anb.com.sa"}]',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $plan1 = Plan::create([
            'name' => '3 Months',
            'description' => 'Tenor of 3 months with a rate of 1% per month.',
            'bank_id' => $anb->id,
            'min_value' => 1000,
            'interest' => 1,
            'plan_code' => '3MonthsANB',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $plan2 = Plan::create([
            'name' => '6 Months',
            'description' => 'Tenor of 6 months with a rate of 1% pet month.',
            'bank_id' => $anb->id,
            'min_value' => 1000,
            'interest' => 1,
            'plan_code' => '6MonthsANB',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $plan3 = Plan::create([
            'name' => '9 Months',
            'description' => 'Tenor of 9 months with a rate of 1% pet month.',
            'bank_id' => $anb->id,
            'min_value' => 1000,
            'interest' => 1,
            'plan_code' => '9MonthsANB',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $plan4 = Plan::create([
            'name' => '12 Months',
            'description' => 'Tenor of 12 months with a rate of 1% pet month.',
            'bank_id' => $anb->id,
            'min_value' => 1000,
            'interest' => 1,
            'plan_code' => '12MonthsANB',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $bajBins = ['420177','455035','466515','455037','473258','520430','520431','536813','542806'];
        foreach ($bajBins as $bin) {
            $bin = Bin::create([
                'bin' => $bin,
                'bank_id' => $anb->id,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
            $plan1->bins()->attach($bin->id);
            $plan2->bins()->attach($bin->id);
            $plan3->bins()->attach($bin->id);
            $plan4->bins()->attach($bin->id);
        }
    }

    private function SABBbank(){
        $sabb = Bank::create([
            'name' => 'Saudi British Bank',
            'status' => 1,
            'country' => 'SA',
            'image' => null, // upload later manually,
            'frequency' => 3,
            'terms' => 'SABB required the full card BIN in the file. <br>
                SABB required to test before switching to live. <br>
                Find in the attachment transaction’s file.  <br>
                SABB required the file after 48 hrs of conducting the transaction. <br>
                Cardholder should agree on the terms and conditions of the issuer bank before conducting the transaction.(attached)',
            'email' => '[{"email":"sabbcard@sabb.com"},{"email":"raed.alzahrani@sabb.com"},{"email":"abdulazizalmusalmani@sabb.com"}]',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $plan1 = Plan::create([
            'name' => '3 Months',
            'description' => 'Tenor of 3 months with a minimum amount of 1000 and not more than 95% of the total card limit with 0 fees.',
            'bank_id' => $sabb->id,
            'min_value' => 1000,
            'interest' => 0,
            'fees' => 0,
            'plan_code' => '3MonthsSABB',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $plan2 = Plan::create([
            'name' => '6 Months',
            'description' => 'Tenor of 6 months with a minimum amount of 1000 and not more than 95% of the total card limit with 99 SAR fees per month excluded VAT.',
            'bank_id' => $sabb->id,
            'min_value' => 1000,
            'interest' => 0,
            'fees' => 99,
            'plan_code' => '6MonthsSABB',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $plan3 = Plan::create([
            'name' => '12 Months',
            'description' => 'Tenor of 12 months with a minimum amount of 2000 and not more than 95% of the total card limit with 199 SAR fees per month excluded VAT.',
            'bank_id' => $sabb->id,
            'min_value' => 2000,
            'interest' => 0,
            'fees' => 199,
            'plan_code' => '12MonthsSABB',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $plan4 = Plan::create([
            'name' => '24 Months',
            'description' => 'Tenor of 24 months with a minimum amount of 5000 and not more than 95% of the total card limit with 50 SAR fees and 24% interest rate per month excluded VAT.',
            'bank_id' => $sabb->id,
            'min_value' => 5000,
            'interest' => 24,
            'fees' => 50,
            'plan_code' => '24MonthsSABB',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $bajBins = ['427222','439277','456893','433786','456891','549799','512060','546755','543199','546756','546757','540236','547645'];
        foreach ($bajBins as $bin) {
            $bin = Bin::create([
                'bin' => $bin,
                'bank_id' => $sabb->id,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
            $plan1->bins()->attach($bin->id);
            $plan2->bins()->attach($bin->id);
            $plan3->bins()->attach($bin->id);
            $plan4->bins()->attach($bin->id);
        }
    }

    private function NBDbank(){
        $sabb = Bank::create([
            'name' => 'Emirates NBD',
            'status' => 1,
            'country' => 'UAE',
            'image' => null, // upload later manually,
            'frequency' => 3,
            'terms' => null,
            'email' => '[{"email":"MarwanALD@EmiratesNBD.com"},{"email":"RubaA@emiratesnbd.com"},{"email":"SultanaAL@emiratesnbd.com"},{"email":"MashhourA@EmiratesNBD.com"}]',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $plan1 = Plan::create([
            'name' => '3 Months',
            'description' => 'Tenor of 3 months with a rate of 1.5% per month.',
            'bank_id' => $sabb->id,
            'min_value' => 0,
            'interest' => 1.5,
            'fees' => 0,
            'plan_code' => '3MonthsNBD',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $plan2 = Plan::create([
            'name' => '6 Months',
            'description' => ' Tenor of 6 months with a rate of 1.5% pet month.',
            'bank_id' => $sabb->id,
            'min_value' => 0,
            'interest' => 1.5,
            'fees' => 0,
            'plan_code' => '6MonthsNBD',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $plan3 = Plan::create([
            'name' => '9 Months',
            'description' => 'Tenor of 9 months with a rate of 1.5% pet month.',
            'bank_id' => $sabb->id,
            'min_value' => 0,
            'interest' => 1.5,
            'fees' => 0,
            'plan_code' => '9MonthsNBD',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $plan4 = Plan::create([
            'name' => '12 Months',
            'description' => 'Tenor of 12 months with a rate of 1.5% pet month.',
            'bank_id' => $sabb->id,
            'min_value' => 0,
            'interest' => 1.5,
            'fees' => 0,
            'plan_code' => '12MonthsNBD',
            'created_at' => now(),
            'updated_at' => now(),
        ]);


        $bajBins = ['410682','410683','410684','458263'];
        foreach ($bajBins as $bin) {
            $bin = Bin::create([
                'bin' => $bin,
                'bank_id' => $sabb->id,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
            $plan1->bins()->attach($bin->id);
            $plan2->bins()->attach($bin->id);
            $plan3->bins()->attach($bin->id);
            $plan4->bins()->attach($bin->id);
        }
    }
}

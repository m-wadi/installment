<?php

namespace Database\Seeders;

use App\Models\Merchant;
use App\Models\PaymentSetting;
use Illuminate\Database\Seeder;

class MerchantAndPaymentSettingTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $huawei = Merchant::create([
            'name' => 'Huawei',
            'secret' => 'HuaweiTEST',
            'whitelist_ip' => '[{"ip":"213.139.32.133"},{"ip":"103.218.216.69"},{"ip":"103.218.216.70"},{"ip":"103.218.216.71"},{"ip":"103.218.216.72"},{"ip":"113.200.78.44"},{"ip":"121.36.72.193"}]',
            'mid' => 'testMID'
        ]);
    }
}

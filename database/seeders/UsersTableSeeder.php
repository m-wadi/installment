<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Ibraheem Jarrar',
            'email' => 'ibraheem.jarrar@hyperpay.com',
            'password' => bcrypt('123456'),
            'created_at' => now(),
            'updated_at' => now()
        ])->assignRole('Admin');

        User::create([
            'name' => 'Mohammad Wadi',
            'email' => 'mohammad.wadi@hyperpay.com',
            'password' => bcrypt('123456'),
            'created_at' => now(),
            'updated_at' => now()
        ])->assignRole('Admin');

        User::create([
            'name' => 'Tariq Atari',
            'email' => 'tariq.atari@hyperpay.com',
            'password' => bcrypt('123456'),
            'created_at' => now(),
            'updated_at' => now()
        ])->assignRole('Admin');
    }
}

// handel click on bank
$('.bnk-imgs').click(function () {
    hideError();
    removeSelectedBanks()
    $(this).addClass('selected')
    let params = 'merchantBankId=' + this.dataset.merchantbankid + '&merchantId=' + this.dataset.merchantid
    callAjax(params, 'api/v1/getMerchantPlans', 'displayPlans');
});

function displayPlans(data) {
    $(".plans").empty();

    let table = document.createElement('table');
    table.classList.add('plansTable');

    for (const element of data['plans']) {
        let tr = document.createElement('tr');
        tr.classList.add('plansRow');
        tr.addEventListener('click', planClick)
        let planNameCell = document.createElement('td');
        let planDescCell = document.createElement('td');
        let planInputCell = document.createElement('td');

        let planName = document.createTextNode(element['plan']['name']);

        let descLocale = 'description_' + local
        let planeDesc = document.createTextNode(element['plan']['description'][descLocale]);
        // planNameCell.appendChild(planName);
        planDescCell.appendChild(planeDesc);

        let planInput = document.createElement("INPUT");
        planInput.setAttribute("type", "hidden");
        planInput.setAttribute("name", "merchantPlanId");
        planInput.setAttribute("value", element['id']);
        planInputCell.appendChild(planInput);

        tr.appendChild(planInputCell);
        tr.appendChild(planNameCell);
        tr.appendChild(planDescCell);

        table.appendChild(tr);
    }

    let bankNameLocale = getBankName(data['bankName']);
    let plansTextLocale = local == 'en' ? 'Plans for' : 'خطط ';
    $(".plans").append('<hr><h4 class="mb-2">  ' + plansTextLocale + ' ' + bankNameLocale + '</h4>');
    $(".plans").append(table).show()
}

function planClick() {
    removeSelectedPlans()
    $(this).addClass('selected')
    $(".paymentForm").show();
    document.getElementById("paymentForm").scrollIntoView();

}

function removeSelectedPlans() {
    $(".plansRow").each(function (index) {
        $(this).removeClass('selected');
    });
}

function removeSelectedBanks() {
    $(".bnk-imgs").each(function (index) {
        $(this).removeClass('selected');
    });
}

function removeInputErrors() {
    $(".paymentForm-input-error").each(function (index) {
        $(this).removeClass('paymentForm-input-error');
        $(this).parent().find("small").remove()

    });
}

function callAjax(params, endPoint, callbackFunction) {
    $.ajax({
        type: 'GET',
        url: '/' + endPoint + '?' + params,
        dataType: 'json',
        success: function (data) {
            window[callbackFunction](data);
            return data;
        }, error: function (data) {
            showError(data.responseJSON)
        }
    });
}

$("#paymentButton").click(function () {
    removeInputErrors();
    let selectedRow = $(".plansRow.selected");
    if (selectedRow.length === 0) {
        showError('Error : No plans selected');
    }

    let merchantPlanId = selectedRow.find('input[name="merchantPlanId"]').val();
    let merchantBankId = $(".bnk-imgs.selected")[0].dataset.merchantbankid;
    let merchantId = $(".bnk-imgs.selected")[0].dataset.merchantid;
    let cardnumber = $("#cardnumber").val()
    let expirationdate = $("#expirationdate").val()
    let securitycode = $("#securitycode").val()
    let name = $("#name").val()

    if (!name) {
        showInputError($("#name"), 'Card holder name is required')
        return;
    }
    if (!cardnumber) {
        showInputError($("#cardnumber"), 'Card number is required')
        return;
    }
    if (!expirationdate) {
        showInputError($("#expirationdate"), 'Card expiry is required')
        return;
    }
    if (!securitycode) {
        showInputError($("#securitycode"), 'CVV is required')
        return;
    }

    data = {
        cardnumber,
        name,
        expirationdate,
        securitycode,
    }

    callPostAjax(data, 'api/v1/validatePaymentFrom', 'validatePaymentForm')
});

function hideError() {
    $("#errorClass").text('').hide();
}

function showError(error) {
    $("#errorClass").html(error).show();
    window.scrollTo(0, 0);
}

function showInputError(input, error) {
    $(input).addClass('paymentForm-input-error');
    $(input).parent().append("<small class=\"text-danger\">" + error + "</small><br>")
}

window.onerror = function (e) {
    showError('Unexpected error occurred')
}

function callPostAjax(data, endPoint, callbackFunction) {
    $.ajax({
        type: 'POST',
        url: '/' + endPoint,
        dataType: 'json',
        data: data,
        success: function (data) {
            window[callbackFunction](data);
            return data;
        }, error: function (data) {
            showError(data.responseJSON)
        }
    });
}

function validatePaymentForm(data) {
    if (data != 'success') {
        showError('Unexpected error occurred')
        return;
    }

    // make form and submit it in js
    removeInputErrors();
    let selectedRow = $(".plansRow.selected");
    if (selectedRow.length === 0) {
        showError('Error : No plans selected');
    }

    let merchantPlanId = selectedRow.find('input[name="merchantPlanId"]').val();
    let merchantBankId = $(".bnk-imgs.selected")[0].dataset.merchantbankid;
    let merchantId = $(".bnk-imgs.selected")[0].dataset.merchantid;
    let cardnumber = $("#cardnumber").val()
    let expirationdate = $("#expirationdate").val()
    let securitycode = $("#securitycode").val()
    let paymentSessionToken = $("#paymentSessionToken").val();
    let name = $("#name").val()

    if (!name) {
        showInputError($("#name"), 'Card holder name is required')
        return;
    }
    if (!cardnumber) {
        showInputError($("#cardnumber"), 'Card holder name is required')
        return;
    }
    if (!expirationdate) {
        showInputError($("#expirationdate"), 'Card holder name is required')
        return;
    }
    if (!securitycode) {
        showInputError($("#securitycode"), 'Card holder name is required')
        return;
    }

    let paymentForm = document.createElement("form");

    let paymentSessionTokenInput = document.createElement("input");
    paymentSessionTokenInput.value = paymentSessionToken;
    paymentSessionTokenInput.name = "paymentSessionToken";
    paymentForm.appendChild(paymentSessionTokenInput);

    let merchantPlanIdInput = document.createElement("input");
    merchantPlanIdInput.value = merchantPlanId;
    merchantPlanIdInput.name = "merchantPlanId";
    paymentForm.appendChild(merchantPlanIdInput);

    let merchantBankIdInput = document.createElement("input");
    merchantBankIdInput.value = merchantBankId;
    merchantBankIdInput.name = "merchantBankId";
    paymentForm.appendChild(merchantBankIdInput);

    let merchantIdInput = document.createElement("input");
    merchantIdInput.value = merchantId;
    merchantIdInput.name = "merchantId";
    paymentForm.appendChild(merchantIdInput);

    let cardnumberInput = document.createElement("input");
    cardnumberInput.value = cardnumber;
    cardnumberInput.name = "cardnumber";
    paymentForm.appendChild(cardnumberInput);

    let expirationdateInput = document.createElement("input");
    expirationdateInput.value = expirationdate;
    expirationdateInput.name = "expirationdate";
    paymentForm.appendChild(expirationdateInput);

    let securitycodeInput = document.createElement("input");
    securitycodeInput.value = securitycode;
    securitycodeInput.name = "securitycode";
    paymentForm.appendChild(securitycodeInput);

    let nameInput = document.createElement("input");
    nameInput.value = name;
    nameInput.name = "name";
    paymentForm.appendChild(nameInput);

    let _tokenInput = document.createElement("input");
    _tokenInput.value = _token;
    _tokenInput.name = "_token";
    paymentForm.appendChild(_tokenInput);

    let colorDepth = screen.colorDepth;
    let language = window.navigator.userLanguage || window.navigator.language;
    let height = screen.height;
    let width = screen.width;
    let timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
    let userAgent = navigator.userAgent;
    let challengeWindow = 5;

    let colorDepthInput = document.createElement("input");
    colorDepthInput.value = colorDepth;
    colorDepthInput.name = "colorDepth";
    paymentForm.appendChild(colorDepthInput);

    let languageInput = document.createElement("input");
    languageInput.value = language;
    languageInput.name = "language";
    paymentForm.appendChild(languageInput);

    let heightInput = document.createElement("input");
    heightInput.value = height;
    heightInput.name = "height";
    paymentForm.appendChild(heightInput);

    let widthInput = document.createElement("input");
    widthInput.value = width;
    widthInput.name = "width";
    paymentForm.appendChild(widthInput);

    let timeZoneInput = document.createElement("input");
    timeZoneInput.value = timeZone;
    timeZoneInput.name = "timeZone";
    paymentForm.appendChild(timeZoneInput);

    let userAgentInput = document.createElement("input");
    userAgentInput.value = userAgent;
    userAgentInput.name = "userAgent";
    paymentForm.appendChild(userAgentInput);

    let challengeWindowInput = document.createElement("input");
    challengeWindowInput.value = challengeWindow;
    challengeWindowInput.name = "challengeWindow";
    paymentForm.appendChild(challengeWindowInput);

    paymentForm.method = "POST";
    paymentForm.action = "/middlePage/submit";

    document.body.appendChild(paymentForm);

    paymentForm.submit();
}

function insertParam(key, value) {
    key = encodeURIComponent(key);
    value = encodeURIComponent(value);

    // kvp looks like ['key1=value1', 'key2=value2', ...]
    var kvp = document.location.search.substr(1).split('&');
    let i = 0;

    for (; i < kvp.length; i++) {
        if (kvp[i].startsWith(key + '=')) {
            let pair = kvp[i].split('=');
            pair[1] = value;
            kvp[i] = pair.join('=');
            break;
        }
    }

    if (i >= kvp.length) {
        kvp[kvp.length] = [key, value].join('=');
    }

    // can return this or...
    let params = kvp.join('&');

    // reload page with new params
    document.location.search = params;
}

function getBankName(bankName) {
    let arr = search(bankName, data)
    if (arr) {
        return arr.values[local]
    }
}

function search(nameKey, myArray) {
    for (var i = 0; i < myArray.length; i++) {
        if (myArray[i].key === nameKey) {
            return myArray[i];
        }
    }
}


let data = [
    {
        key: "Emirates NBD",
        values: {
            'ar': "بنك الامارات دبي الوطني",
            'en': "Emirates NBD"
        }
    },
    {
        key: "Saudi British Bank",
        values: {
            'ar': "البنك السعودي البريطاني",
            'en': "Saudi British Bank"
        }
    },
    {
        key: "Arab National Bank",
        values: {
            'ar': "البنك العربي الوطني",
            'en': "Arab National Bank"
        }
    },
    {
        key: "Bank Al Jazera",
        values: {
            'ar': "بنك الجزيرة",
            'en': "Bank Al Jazera"
        }
    }
];



<?php

use App\Http\Controllers\API\MiddlePageController;
use App\Http\Controllers\API\TokenController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::middleware('api')->group(function () {
    Route::post('v1/generateToken', [TokenController::class, 'generateToken']);
    Route::get('v1/getMerchantPlans', [MiddlePageController::class, 'getMerchantPlans']);
    Route::post('v1/validatePaymentFrom', [MiddlePageController::class, 'validatePaymentFormRequest']);
});

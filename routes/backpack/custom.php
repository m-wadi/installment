<?php

use Illuminate\Support\Facades\Route;

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => array_merge(
        (array) config('backpack.base.web_middleware', 'web'),
        (array) config('backpack.base.middleware_key', 'admin')
    ),
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::crud('merchant', 'MerchantCrudController');
    Route::crud('payment-setting', 'PaymentSettingCrudController');
    Route::crud('bank', 'BankCrudController');
    Route::crud('plan', 'PlanCrudController');
    Route::crud('bin', 'BinCrudController');
    Route::crud('transaction', 'TransactionCrudController');
    Route::crud('merchant-bin', 'MerchantBinCrudController');
    Route::crud('merchant-bank', 'MerchantBankCrudController');
    Route::crud('merchant-plan', 'MerchantPlanCrudController');
    Route::get('charts/transaction-banks', 'Charts\TransactionBanksChartController@response')->name('charts.transaction-banks.index');
    Route::get('charts/transaction-status', 'Charts\TransactionStatusChartController@response')->name('charts.transaction-status.index');
}); // this should be the absolute last line of this file
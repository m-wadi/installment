<?php

use App\Http\Controllers\MiddlePageController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/admin');
});

Route::get('plans/redirect/{token}',[MiddlePageController::class,'showMiddlePage']);
Route::post('middlePage/submit',[MiddlePageController::class,'paymentRequest']);

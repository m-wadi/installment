<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class MerchantBank extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'merchant_banks';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function addMerchantPlansButton()
    {
        return '<a class="btn btn-sm btn-link btn-outline" data-button-type="send" href="' . url('admin/merchant-plan/create?merchantBankId=' . $this->id) . '" data-toggle="tooltip" title="Add Plans"><i class="la la-plus"></i> Add Plans</a>';
    }

    public function listMerchantPlansButton()
    {
        return '<a class="btn btn-sm btn-link btn-outline" data-button-type="send" href="' . url('admin/merchant-plan?merchantBankId=' . $this->id) . '" data-toggle="tooltip" title="List Plans"><i class="la la-eye"></i> Merchant Plans</a>';

    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function merchant()
    {
        return $this->belongsTo(Merchant::class);
    }

    public function bank()
    {
        return $this->belongsTo(Bank::class,'bank_id');
    }

    public function plans()
    {
        return $this->hasMany(MerchantPlan::class,'merchant_banks_id');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}

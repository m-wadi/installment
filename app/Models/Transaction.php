<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use App\Encryption\Crypto;

class Transaction extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'transactions';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function storeTransaction($data)
    {
        return Transaction::create([
            'currency' => $data['currency'],
            'amount' => $data['amount'],
            'merchant_transaction_id' => $data['merchantTransactionId'],
            'merchant_id' => $data['merchantId'],
            'shopper_result_url' => $data['shopperResultUrl'],
            'payment_setting_id' => $data['paymentSettingId']
        ]);
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function merchant()
    {
        return $this->belongsTo(Merchant::class);
    }

    public function paymentSetting()
    {
        return $this->belongsTo(PaymentSetting::class);
    }

    public function merchantPlan()
    {
        return $this->belongsTo(MerchantPlan::class,'merchant_plan_id');
    }

    public function sessionToken()
    {
        return $this->hasOne(SessionToken::class,'transaction_id');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */
    public function getCardAttribute($value)
    {
        return Crypto::decrypt($value, env('CARD_ENCRYPTION_KEY'), true);
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function setCardAttribute($value)
    {
        $this->attributes['card'] = Crypto::encrypt($value, env('CARD_ENCRYPTION_KEY'), true);
    }
}

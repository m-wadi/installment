<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class MerchantPlan extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'merchant_plans';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function addMerchantBinsButton()
    {
        return '<a class="btn btn-sm btn-link btn-outline" data-button-type="send" href="' . url('admin/merchant-bin/create?merchantPlanId=' . $this->id) . '" data-toggle="tooltip" title="Add Bins"><i class="la la-plus"></i> Add Bins</a>';
    }

    public function listMerchantBinsButton()
    {
        return '<a class="btn btn-sm btn-link btn-outline" data-button-type="send" href="' . url('admin/merchant-bin?merchantPlanId=' . $this->id) . '" data-toggle="tooltip" title="List Bins"><i class="la la-eye"></i> Merchant Bins</a>';

    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function plan()
    {
        return $this->belongsTo(Plan::class);
    }

    public function merchantBank()
    {
        return $this->belongsTo(MerchantBank::class,'merchant_banks_id');
    }

    public function bins()
    {
        return $this->hasMany(MerchantBin::class,'merchant_plans_id');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}

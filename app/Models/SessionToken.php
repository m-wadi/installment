<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SessionToken extends Model
{
    use HasFactory;

    protected $table = 'session_tokens';
    protected $fillable = ['token', 'transaction_id', 'expires_at', 'is_used'];

    public static function generate($transaction)
    {
        $string = $transaction->merchant_transaction_id . time() . $transaction->merchant_id;
        $generated_token = sha1($string);
        return SessionToken::create([
            'token' => $generated_token,
            'transaction_id' => $transaction->id,
            'expires_at' => now()->addMinutes(30)
        ]);
    }

    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }
}

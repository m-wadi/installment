<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;


class Bank extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'banks';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function bins()
    {
        return $this->hasMany(Bin::class);
    }

    public function bankJob()
    {
        return $this->hasOne(BankJob::class ,'bank_id');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */
    public function getArabicPhone($value): string
    {
        $split = explode('-',$value);
        return $split[2] . '-' . $split[1] . '-' . $split[0];
    }
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function setImageAttribute($value)
    {
        $attribute_name = "image";
        if (empty($value)) {
            $this->attributes[$attribute_name] = '';
        } else {
            $disk = "public";
            $destination_path = "images/banks";

            // if the image was erased
            if ($value == null) {
                // delete the image from disk
                \Storage::disk($disk)->delete($this->{$attribute_name});

                // set null in the database column
                $this->attributes[$attribute_name] = null;
            }
            // if a base64 was sent, store it in the db
            if (Str::startsWith($value, 'data:image')) {
                $extension = explode('/', mime_content_type($value))[1];
                // 0. Make the image
                $image = \Image::make($value);
                // 1. Generate a filename.

                $filename = md5($value . time()) . '.' . $extension;
                // 2. Store the image on disk.
                \Storage::disk($disk)->put('uploads/' . $destination_path . '/' . $filename, $image->stream());
                // 3. Save the path to the database
                $this->attributes[$attribute_name] = 'uploads/' . $destination_path . '/' . $filename;
            }
        }
    }
}

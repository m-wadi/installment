<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BankJob extends Model
{
    use HasFactory;
    protected $fillable = ['bank_id','last_run'];
    protected $table = 'bank_jobs';
}

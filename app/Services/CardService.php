<?php

namespace App\Services;

class CardService
{
    public function validateCardNumber($number): bool
    {
        $number = str_replace(' ', '', $number);
        // Set the string length and parity
        $number_length = strlen($number);
        if ($number_length < 15 || $number_length > 16) {
            return false;
        }

        //luhnCheck
        $parity = $number_length % 2;

        // Loop through each digit and do the maths
        $total = 0;
        for ($i = 0; $i < $number_length; $i++) {
            $digit = $number[$i];
            // Multiply alternate digits by two
            if ($i % 2 == $parity) {
                $digit *= 2;
                // If the sum is two digits, add them together (in effect)
                if ($digit > 9) {
                    $digit -= 9;
                }
            }
            // Total up the digits
            $total += $digit;
        }

        // If the total mod 10 equals 0, the number is valid
        return $total % 10 == 0;
    }

    public function validateExpiryDate($expiration_date): bool
    {
        $ex_date = preg_split("#/#", $expiration_date);
        $ex_date = array_map(function ($x) {
            return (int)$x;
        }, $ex_date);

        $current_month = (int)now()->format('n');
        $current_year = (int)now()->format('y');

        $month = $ex_date[0];
        $year = $ex_date[1];

        if ($year == $current_year && $month < $current_month) {
            return false;
        } elseif ($year < $current_year) {
            return false;
        }

        return true;
    }

    public function validateCVV($cvv): bool
    {
        //TODO : fix cvv
        if (!preg_match('/^[0-9]{3,4}$/', $cvv)) {
            return false;
        }
        return true;
    }

    public function detectedCardBrand($cardNumber)
    {
        $cardNumber = str_replace(' ', '', $cardNumber);
        $cardNumber = preg_replace('/\D/', '', $cardNumber);

        // get card bin
        $cardBin = substr($cardNumber, 0, 6);
        // get mada bins
        $madaBins = $this->getMadaBins();

        // TODO : discover cards
        switch ($cardNumber) {
            case(in_array($cardBin, $madaBins)):
                return 'MADA';
            case(preg_match('/^4/', $cardNumber) >= 1):
                return 'VISA';
            case(preg_match('/^(5[1-5][0-9]{2})|2(22[1-9]|2[3-9][0-9]|[3-6][0-9][0-9]|7[0-1][0-9]|720)[0-9]{12}/', $cardNumber) >= 1):
                return 'MASTER';
            case(preg_match('/^3[47]/', $cardNumber) >= 1):
                return 'AMEX';
            default:
                return false;
        }
    }

    public function getCardBin($cardNumber)
    {
        $cardNumber = str_replace(' ', '', $cardNumber);
        $cardNumber = preg_replace('/\D/', '', $cardNumber);

        // get card bin
        return substr($cardNumber, 0, 6);
    }

    private function getMadaBins(): array
    {
        return array(
            "588845", "440647", "440795", "446404", "457865", "968208", "588846", "493428", "539931", "558848", "557606", "968210", "636120", "417633", "468540", "468541", "468542", "468543", "968201", "446393", "409201", "458456", "484783", "968205", "462220", "455708", "588848", "455036", "968203", "486094", "486095", "486096", "504300", "440533", "489318", "489319", "445564", "968211", "401757", "410685", "406996", "432328", "428671", "428672", "428673", "968206", "446672", "543357", "434107", "407197", "407395", "412565", "431361", "604906", "521076", "588850", "968202", "529415", "535825", "543085", "524130", "554180", "549760", "588849", "968209", "524514", "529741", "537767", "535989", "536023", "513213", "520058", "585265", "588983", "588982", "589005", "508160", "531095", "530906", "532013", "605141", "968204", "422817", "422818", "422819", "428331", "483010", "483011", "483012", "589206", "968207", "419593", "439954", "530060", "531196"
        );
    }

    public function splitExpiryDate($expiryDate)
    {
        $expiryDate = preg_split("#/#", $expiryDate);
        // change year from 2 digits to 4 digits. for example (22 => 2022)
        $expiryDate[1] = substr(now()->year, 0, 2) . $expiryDate[1];
        return $expiryDate;
    }

    public function stripWhiteSpacesInCard($cardNumber)
    {
        return str_replace(' ', '', $cardNumber);
    }

    public function prepareCardData($cardNumber, $expiryDate, $name, $cvv): array
    {
        $expiryDateArr = $this->splitExpiryDate($expiryDate);
        return [
            'cvv' => $cvv,
            'name' => $name,
            'expiryMonth' => $expiryDateArr[0],
            'expiryYear' => $expiryDateArr[1],
            'cardNumber' => $this->stripWhiteSpacesInCard($cardNumber),
            'brand' => $this->detectedCardBrand($cardNumber)
        ];
    }
}

<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class PaymentService
{
    private $sessionToken;
    private $cardData;
    private $paymentData;
    private $accessToken;
    private $url;
    private $reportingUrl;
    private $threeDsecure;

    public function __construct($sessionToken, $cardData = [], $threeDsecure = [])
    {
        $this->sessionToken = $sessionToken;
        $this->cardData = $cardData;
        $this->threeDsecure = $threeDsecure;
        $this->paymentData = null;
        $this->accessToken = null;
        $this->url = env('APP_ENV') == 'prod' ? env('LIVE_OPPWA_URL') : env('TEST_OPPWA_URL');
        $this->reportingUrl = env('APP_ENV') == 'prod' ? env('LIVE_OPPWA_REPORTING_URL') : env('TEST_OPPWA_REPORTING_URL');
    }

    public function preparePaymentData()
    {
        $sessionToken = $this->sessionToken;
        $transaction = $sessionToken->transaction;
        $paymentSetting = $transaction->paymentSetting;
        $amount = number_format((float)$transaction->amount, 2, '.', '');
        $currency = $paymentSetting->currency;
        $merchant_transaction_id = $transaction->merchant_transaction_id;
        $brand = $this->cardData['brand'];
        $cardNumber = $this->cardData['cardNumber'];
        $cardHolder = $this->cardData['name'];
        $expiryMonth = $this->cardData['expiryMonth'];
        $expiryYear = $this->cardData['expiryYear'];
        $cvv = $this->cardData['cvv'];
        $planCode = $sessionToken->transaction->merchant->banks->first()->plans->first()->plan->plan_code;

        $data = [
            "entityId" => $paymentSetting->entity_id,
            "merchantTransactionId" => $merchant_transaction_id,
            "amount" => $amount,
            "currency" => $currency,
            "paymentBrand" => $brand,
            "paymentType" => $paymentSetting->payment_type,
            "card.number" => $cardNumber,
            "card.holder" => $cardHolder,
            "card.expiryMonth" => $expiryMonth,
            "card.expiryYear" => $expiryYear,
            "card.cvv" => $cvv,
            "shopperResultUrl" => $transaction->shopper_result_url,
            "customParameters[installment]" => 1,
            "customParameters[Installments_plans]" => $planCode
        ];

        if (!empty($this->threeDsecure)) {
            $data['customer.ip'] = $this->threeDsecure['ip'];
            $data['customer.browser.acceptHeader'] = $this->threeDsecure['acceptHeaders'];
            $data['customer.browser.screenColorDepth'] = $this->threeDsecure['colorDepth'];
            $data['customer.browser.language'] = $this->threeDsecure['language'];
            $data['customer.browser.screenHeight'] = $this->threeDsecure['height'];
            $data['customer.browser.screenWidth'] = $this->threeDsecure['width'];
            $data['customer.browser.timezone'] = $this->threeDsecure['timeZone'];
            $data['customer.browser.userAgent'] = $this->threeDsecure['userAgent'];
            $data['customer.browser.challengeWindow'] = $this->threeDsecure['challengeWindow'];
            $data['customer.browser.javaEnabled'] = $this->threeDsecure['javaEnabled'];
        }

        if (env('APP_ENV') != 'prod') {
            $data['testMode'] = 'EXTERNAL';
        }

        $this->paymentData = $data;
        $this->accessToken = $paymentSetting->access_token;
    }

    public function performPaymentRequest()
    {
        $response = Http::withToken($this->accessToken)->asForm()->post(
            $this->url,
            $this->paymentData
        );
        return json_decode($response->body());
    }

    public function buildRedirectForm($response)
    {
        $form = '<form id="redForm" action="' . $response->redirect->url . '" method="POST">';
        foreach ($response->redirect->parameters as $param) {
            $form .= '<input hidden name="' . $param->name . '" value="' . $param->value . '"/>';
        }
        $form .= '</form>';
        $form .= '<script> document.getElementById("redForm").submit();  </script>';
        return $form;
    }

    public function prepareTransactionReportData()
    {
        $sessionToken = $this->sessionToken;
        $transaction = $sessionToken->transaction;
        $paymentSetting = $transaction->paymentSetting;
    }

    public function transactionReport()
    {
        $sessionToken = $this->sessionToken;
        $transaction = $sessionToken->transaction;
        $paymentSetting = $transaction->paymentSetting;
        $url = $this->reportingUrl . "/" . $transaction->unique_id;
        $url .= "?entityId=" . $paymentSetting->entity_id;

        $response = Http::withToken($paymentSetting->access_token)->get($url);
        return json_decode($response->body());
    }
}

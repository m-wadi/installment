<?php

namespace App\Console\Commands;

use App\Mail\BankTransactionsEmail;
use App\Models\Bank;
use App\Models\BankJob;
use App\Models\MerchantBank;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;

class GenerateBankTransactionFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:BankFiles';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // TODO : create logging about he files
        Log::channel('bankFile')->info('GenerateBankTransactionFile command');
        $banks = Bank::with('bankJob')->where('status', 1)->get();

        foreach ($banks as $bank) {
            $bankCode = $bank->code ?? '';
            $bankFreq = $bank->frequency ?? '24';
            if (empty($bank->bankJob)) {
                BankJob::create([
                    'bank_id' => $bank->id,
                    'last_run' => now()
                ]);
                Log::channel('bankFile')->info('bank : ' . $bank->code . ' ended in no bankJob if');
                continue;
            }
            Log::channel('bankFile')->info('GenerateBankTransactionFile : ' . $bank->code);

            if (Carbon::parse($bank->bankJob->last_run)->diffInHours(now()) < $bankFreq) {
                Log::channel('bankFile')->info('bank : ' . $bank->code . ' ended in last run');
                continue;
            }

            $bankTransactions = $this->getBankTransactions($bank);
            if (empty($bankTransactions)) {
                Log::channel('bankFile')->info('bank : ' . $bank->code . ' ended in no transactions if');
                continue;
            }

            $fileName = "Installments_{$bankCode}_TransactionBankExport.xlsx";

            $exportClass = 'App\Exports\TransactionBankExport' . $bankCode;
            if (!class_exists($exportClass)) {
                Log::channel('bankFile')->info('bank : ' . $bank->code . ' class ' . $exportClass . ' does not exists');
                Log::error('GenerateBankTransactionFile ' . $exportClass . ' does not exists');
                continue;
            }

            Excel::store(new $exportClass($bankTransactions), $bankCode . '/' . $fileName);

            $emailReceivers = json_decode($bank->email, true);
            $emailReceivers = array_column($emailReceivers, 'email');
            $cc = [];
            if (env('TEST_MAIL')) {
                $emailReceivers = 'ibraheem.jarrar@hyperpay.com';
                $cc = [
                    'ibraheem.jarrar@hyperpay.com',
                ];
            }
            Log::channel('bankFile')->info('bank : ' . $bankCode . ' email receivers : ' . json_encode($emailReceivers) . ' and cc: ' . json_encode($cc));

            Mail::to($emailReceivers)->cc($cc)
                ->send(new BankTransactionsEmail($fileName, $bankCode));

            if (empty(Mail::failures())) {
                // email sent successfully
                Log::channel('bankFile')->info('GenerateBankTransactionFile email sent successfully for bank : ' . $bankCode);
                Transaction::whereIn('id', $bankTransactions->pluck('id')->toArray())->update([
                    'is_sent' => 1
                ]);
                $bank->bankJob->update([
                    'last_run' => now()
                ]);
            } else {
                Log::channel('bankFile')->info('GenerateBankTransactionFile email failed for bank : ' . $bankCode);
                Log::channel('bankFile')->info('GenerateBankTransactionFile email failed errors' . implode(',', Mail::failures()));
                Log::error(implode(',', Mail::failures()));
            }

        }
        return 0;
    }

    private function getBankTransactions($bank)
    {
        $merchantBanks = MerchantBank::where('bank_id', $bank->id)->with('plans')->first();
        if (empty($merchantBanks)) {
            return null;
        }
        $merchantPlanIds = $merchantBanks->plans->pluck('id')->toArray() ?? [];
        if (empty($merchantPlanIds)) {
            return null;
        }
        $transactions = Transaction::whereIn('merchant_plan_id', $merchantPlanIds)
            ->where('is_sent', 0)
            ->where('status', 'PAID')->get();
        if (empty($transactions) || $transactions->count() < 1) {
            return null;
        }
        return $transactions;
    }
}

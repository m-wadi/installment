<?php

namespace App\Console\Commands;

use App\Jobs\TransactionInquiryAndUpdate;
use App\Models\Transaction;
use Illuminate\Console\Command;

class TransactionInquiry extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transactionInquiry';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $transactions = Transaction::where('status','PENDING')->with('sessionToken')->get();

        foreach ($transactions as $transaction) {
            echo $transaction->id . ' - ' . $transaction->sessionToken->token . PHP_EOL;
            TransactionInquiryAndUpdate::dispatch($transaction->sessionToken);
        }
    }
}

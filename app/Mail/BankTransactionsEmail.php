<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class BankTransactionsEmail extends Mailable
{
    use Queueable, SerializesModels;

    private $filePath;
    private $filename;
    private $bankCode;

    /**
     * Create a new message instance.
     *
     * @param $filename
     * @param $bankCode
     * @return void
     */
    public function __construct($filename, $bankCode)
    {
        $this->filePath = storage_path() . '/app/' . $bankCode . '/' . $filename;
        $this->bankCode = $bankCode;
        $this->filename = $filename;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        Log::info("in email sent BankTransferEmail");
        return $this->from(env('MAIL_USERNAME'))
            ->subject($this->bankCode.' installment file')
            ->view('emails.transactionBank', [
                'fileName' => $this->filename
            ])->attach($this->filePath, [
                'as' => $this->filename,
                'mime' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            ]);
    }
}

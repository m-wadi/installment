<?php

namespace App\Exports;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class TransactionBankExportNBD implements FromCollection,WithHeadings, WithMapping, WithColumnFormatting, WithColumnWidths
{
    private $transactions;

    public function __construct($transactions)
    {
        $this->transactions = $transactions;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        Log::info('Transaction Bank Export ANB');
        return $this->transactions;
    }

    public function headings(): array
    {
        return [
            'TX ID',
            'TX DATE',
            'TX Time',
            'Authorisation Code',
            'TX Amount',
            'Currency',
            'Credit Card number',
            'Credit card holder name',
            'Selected Plan',
            'Interest Rate',
        ];
    }

    public function map($transaction): array
    {
        $result = json_decode($transaction->result_details);
        $reportResult = json_decode($transaction->report_trx_details);

        return [
            $transaction->merchant->unique_id ?? '',
            Carbon::parse($transaction->created_at)->toDateString() ?? '',
            Carbon::parse($transaction->created_at)->toTimeString() ?? '',
            $reportResult->resultDetails->{'transaction.authorizationCode'} ?? '',
            $transaction->amount ?? '',
            $transaction->paymentSetting->currency ?? '',
            $transaction->card ?? '',
            $result->card->holder ?? '',
            $transaction->merchantPlan->plan->plan_code ?? '',
            $transaction->merchantPlan->plan->interest ?? '',
        ];
    }

    public function columnFormats(): array
    {
        return [
            'G' => NumberFormat::FORMAT_NUMBER,
        ];
    }

    public function columnWidths(): array
    {
        return [
            'G' => '40',
        ];
    }
}

<?php

namespace App\Exports;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class TransactionBankExportBAJ implements FromCollection, WithHeadings, WithMapping, WithColumnFormatting, WithColumnWidths
{
    private $transactions;

    public function __construct($transactions)
    {
        $this->transactions = $transactions;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        Log::info('Transaction Bank Export ANB');
        return $this->transactions;
    }

    public function headings(): array
    {
        return [
            'Card Number',
            'Customer Name',
            'Customer Mobile Number',
            'Customer Email',
            'EMI Tenure',
            'Transaction Amount',
            'Transaction Date',
            'Transaction Time',
            'Merchant Order Reference',
            'Approval Code',
            'Transaction Reference',
            'Business Name',
            'Merchant Account',
            'Transaction Currency',
            'Interest Rate'
        ];
    }

    public function map($transaction): array
    {
        $result = json_decode($transaction->result_details);
        $reportResult = json_decode($transaction->report_trx_details);

        return [
            $transaction->card ?? '',
            $result->card->holder ?? '',
            '', // mobile
            '', // email
            $transaction->merchantPlan->plan->plan_code ?? '',
            $transaction->amount ?? '',
            Carbon::parse($transaction->created_at)->toDateString() ?? '',
            Carbon::parse($transaction->created_at)->toTimeString() ?? '',
            $transaction->merchant_transaction_id ?? '',
            $reportResult->resultDetails->{'transaction.authorizationCode'} ?? '',
            $reportResult->resultDetails->{'transaction.receipt'} ?? '',
            $transaction->merchant->name ?? '',
            $transaction->merchant->mid ?? '',
            $transaction->paymentSetting->currency ?? '',
            $transaction->merchantPlan->plan->interest ?? '',
        ];
    }

    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_NUMBER,
        ];
    }

    public function columnWidths(): array
    {
        return [
            'A' => '40',
        ];
    }
}

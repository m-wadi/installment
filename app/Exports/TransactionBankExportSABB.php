<?php

namespace App\Exports;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class TransactionBankExportSABB implements FromCollection, WithHeadings, WithMapping, WithColumnFormatting, WithColumnWidths
{
    private $transactions;

    public function __construct($transactions)
    {
        $this->transactions = $transactions;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        Log::info('Transaction Bank Export ANB');
        return $this->transactions;
    }

    public function headings(): array
    {
        return [
            'MID',
            'Merchant',
            'Order ID',
            'TX ID',
            'TX Date',
            'TX Time',
            'Authorisation Code',
            'TX Amount',
            'Currency',
            'Credit Card',
            'Selected Plan',
            'Buyer',
            'Interest Rate',
            'Processing Fees'
        ];
    }

    public function map($transaction): array
    {
        $result = json_decode($transaction->result_details);
        $reportResult = json_decode($transaction->report_trx_details);
        return [
            $transaction->merchant->mid ?? '',
            $transaction->merchant->name ?? '',
            $transaction->merchant_transaction_id ?? '',
            $transaction->unique_id ?? '',
            Carbon::parse($transaction->created_at)->toDateString() ?? '',
            Carbon::parse($transaction->created_at)->toTimeString() ?? '',
            $reportResult->resultDetails->{'transaction.authorizationCode'} ?? '',
            $transaction->amount ?? '',
            $transaction->paymentSetting->currency ?? '',
            strval($transaction->card) ?? '',
            $transaction->merchantPlan->plan->plan_code ?? '',
            $result->card->holder ?? '',
            (strval($transaction->merchantPlan->plan->interest) ?? '') . '%',
            (strval($transaction->merchantPlan->plan->fees) ?? '') . $transaction->paymentSetting->currency ?? '',
        ];
    }

    public function columnFormats(): array
    {
        return [
            'J' => NumberFormat::FORMAT_TEXT,
        ];
    }

    public function columnWidths(): array
    {
        return [
            'J' => '40',
        ];
    }
}

<?php

namespace App\Exports;

use App\Models\MerchantBank;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use function Symfony\Component\Translation\t;

class TransactionBankExportANB implements FromCollection, WithHeadings, WithMapping, WithColumnFormatting, WithColumnWidths
{
    private $transactions;

    public function __construct($transactions)
    {
        $this->transactions = $transactions;
    }

    /**
     * @return Collection
     */
    public function collection()
    {
        Log::info('Transaction Bank Export ANB');
        return $this->transactions;
    }

    public function headings(): array
    {
        return [
            'Merchant ID',
            'Merchant',
            'Order ID',
            'TX ID',
            'TX Date',
            'TX Time',
            'Authorization Code',
            'TX Amount',
            'Currency',
            'Credit Card Number',
            'Selected Installments Plan',
            'Bank Plan Code',
            'Interest Rate',
            'Customer name'
        ];
    }

    public function map($transaction): array
    {
        $result = json_decode($transaction->result_details);
        $reportResult = json_decode($transaction->report_trx_details);

        return [
            $transaction->merchant->mid ?? '',
            $transaction->merchant->name ?? '',
            $transaction->merchant_transaction_id ?? '',
            $transaction->unique_id ?? '',
            Carbon::parse($transaction->created_at)->toDateString() ?? '',
            Carbon::parse($transaction->created_at)->toTimeString() ?? '',
            $reportResult->resultDetails->{'transaction.authorizationCode'} ?? '',
            $transaction->amount ?? '',
            $transaction->paymentSetting->currency ?? '',
            $transaction->card ?? '',
            $transaction->merchantPlan->plan->name ?? '',
            $transaction->merchantPlan->plan->plan_code ?? '',
            $transaction->merchantPlan->plan->interest ?? '',
            $result->card->holder ?? '',
        ];
    }

    public function columnFormats(): array
    {
        return [
            'J' => NumberFormat::FORMAT_NUMBER,
        ];
    }

    public function columnWidths(): array
    {
        return [
            'J' => '40',
        ];
    }
}

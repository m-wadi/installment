<?php

namespace App\Jobs;

use App\Services\PaymentService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class TransactionInquiryAndUpdate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $sessionToken;

    /**
     * Create a new job instance.
     *
     * @param $sessionToken
     */
    public function __construct($sessionToken)
    {
        $this->sessionToken = $sessionToken;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info("TransactionInquiryAndUpdate job started");
        $paymentService = new PaymentService($this->sessionToken);
        $response = $paymentService->transactionReport();
        $this->sessionToken->transaction->update([
            'report_trx_details' => json_encode($response)
        ]);
        if ($response->result->code == '000.100.112' || $response->result->code == '000.000.000') {
            $this->sessionToken->transaction->update([
                'status' => 'PAID'
            ]);
        } else {
            $this->sessionToken->transaction->update([
                'status' => 'DECLINED'
            ]);
        }

        Log::info("TransactionInquiryAndUpdate job status update : " . $this->sessionToken->transaction->status);

    }
}

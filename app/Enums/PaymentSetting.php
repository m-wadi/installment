<?php

namespace App\Enums;

class PaymentSetting
{
    public const ENVIRONMENTS = [
        'EXTERNAL' => 'Connector Test',
        'INTERNAL' => 'Integrator Test',
        'LIVE' => 'Live'
    ];

    public const CURRENCIES = [
        'SAR' => 'SAR',
        'AED' => 'AED',
        'JOD' => 'JOD',
        'USD' => 'USD',
        'OMR' => 'OMR',
        'KWD' => 'KWD',
        'BHD' => 'BHD',
        'QAR' => 'QAR',
        'EUR' => 'EUR',
        'EGP' => 'EGP',
        'IQD' => 'IQD'
    ];

    public const PAYMENT_METHODS = [
        'VISA' => 'Visa',
        'MASTER' => 'Mastercard',
        'PAYPAL' => 'PayPal',
        'AMEX' => 'American Express',
        'SADAD' => 'Sadad',
        'MADA' => 'Mada',
        'STC_PAY' => 'STCPAY',
        'APPLEPAY' => 'ApplePay'
    ];

    public const PAYMENT_TYPES = [
        'DB' => 'Debit',
        'PA' => 'Pre Authorization'
    ];

    public const CONNECTORS = [
        'migs' => 'MIGS',
        'visaacp' => 'VISA ACP'
    ];

    public const FORM_STYLES = [
        'card' => 'Card',
        'plain' => 'Plain'
    ];

    public const LANGUAGE_CODES = [
        'ar' => 'Arabic',
        'en' => 'English'
    ];
}

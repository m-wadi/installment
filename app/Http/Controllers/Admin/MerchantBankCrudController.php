<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\MerchantBankRequest;
use App\Models\Bank;
use App\Models\MerchantBank;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\Route;

/**
 * Class MerchantBankCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class MerchantBankCrudController extends CrudController
{
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(MerchantBank::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/merchant-bank');
        CRUD::setEntityNameStrings('merchant bank', 'merchant banks');

        $this->crud->addButtonFromModelFunction('line', 'Add Plans', 'addMerchantPlansButton');
        $this->crud->addButtonFromModelFunction('line', 'List Plans', 'listMerchantPlansButton');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->denyAccess(['show']);

        CRUD::column('merchant_id');
        CRUD::column('bank_id');
        CRUD::column('created_at');
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(MerchantBankRequest::class);

        $this->crud->addField([
            'name' => 'merchant_id',
            'label' => 'Merchant',
            'type' => "relationship",
        ]);

        $this->crud->addField([
            'name' => 'bank',
            'label' => 'Bank',
            'type' => 'select_multiple',
            'entity' => 'bank', // the method that defines the relationship in your Model
            'model' => "App\Models\Bank", // foreign key model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'allows_null' => false
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        CRUD::setValidation(MerchantBankRequest::class);

        $this->crud->addField([
            'name' => 'merchant_id',
            'label' => 'Merchant',
            'type' => "relationship",
        ]);

        $this->crud->addField([
            'name' => 'bank_id',
            'label' => 'Bank',
            'type' => "relationship",
        ]);
    }

    public function store()
    {
        $this->crud->hasAccessOrFail('create');

        // execute the FormRequest authorization and validation, if one is required
        $request = $this->crud->validateRequest();
        $banksIds = $this->crud->getStrippedSaveRequest()['bank'] ?? null;
        $merchantId = $this->crud->getStrippedSaveRequest()['merchant_id'] ?? null;

        if (empty($banksIds)) {
            return redirect()->back()->withErrors("Please make sure to select bank(s)");
        }

        if (empty($merchantId)) {
            return redirect()->back()->withErrors("Please make sure to select a merchant");
        }

        // insert item in the db
        foreach ($banksIds as $bank) {
            $newItem['merchant_id'] = $merchantId;
            $newItem['bank_id'] = $bank;

            if (MerchantBank::where('merchant_id', $merchantId)->where('bank_id', $bank)->exists()) {
                if (end($banksIds) == $bank){
                    return redirect()->back()->withErrors('Bank ' . Bank::select('id', 'name')->where('id', $bank)->first()->name . ' Already assigned to this merchant ');
                }
                \Alert::error('' . Bank::select('id', 'name')->where('id', $bank)->first()->name . ' Already assigned to this merchant ')->flash();
                continue;
            }

            $item = $this->crud->create($newItem);
            $this->data['entry'] = $this->crud->entry = $item;
        }

        // show a success message
        \Alert::success(trans('backpack::crud.insert_success'))->flash();

        // save the redirect choice for next time
        $this->crud->setSaveAction();

        return $this->crud->performSaveAction($item->getKey());
    }
}

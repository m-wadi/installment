<?php

namespace App\Http\Controllers\Admin\Charts;

use App\Models\Transaction;
use Backpack\CRUD\app\Http\Controllers\ChartController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;
use Carbon\Carbon;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;

/**
 * Class TransactionBanksChartController
 * @package App\Http\Controllers\Admin\Charts
 * @property-read CrudPanel $crud
 */
class TransactionBanksChartController extends ChartController
{
    public function setup()
    {
        $this->chart = new Chart();

        // MANDATORY. Set the labels for the dataset points
        $this->chart->labels([
            'Transaction Count',
        ]);

        // RECOMMENDED. Set URL that the ChartJS library should call, to get its data using AJAX.
        $this->chart->load(backpack_url('charts/transaction-banks'));

        // OPTIONAL
         $this->chart->minimalist(false);
         $this->chart->displayLegend(true);
    }

    /**
     * Respond to AJAX calls with all the chart data points.
     *
     * @return json
     */
    public function data()
    {
        $transactionCount = Transaction::query()
            ->selectRaw('SUM(CASE WHEN transactions.status="PENDING" THEN 1 ELSE 0 END) AS pending')
            ->selectRaw('SUM(CASE WHEN transactions.status="PAID" THEN 1 ELSE 0 END) AS paid')
            ->selectRaw('SUM(CASE WHEN transactions.status="DECLINED" THEN 1 ELSE 0 END) AS declined')
            ->first();
        $this->chart->dataset('Paid', 'bar', [$transactionCount->paid])
            ->color('rgba(0, 214, 143, 1)')
            ->backgroundColor('rgba(0, 214, 143, 0.8)');

        $this->chart->dataset('Declined', 'bar', [$transactionCount->declined])
            ->color('rgba(205, 32, 31, 1)')
            ->backgroundColor('rgba(205, 32, 31, 0.8)');

        $this->chart->dataset('Pending', 'bar', [$transactionCount->pending])
            ->color('rgba(255, 204, 0, 1)')
            ->backgroundColor('rgba(255, 204, 0, 0.8)');
    }
}

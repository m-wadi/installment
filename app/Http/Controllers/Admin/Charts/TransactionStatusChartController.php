<?php

namespace App\Http\Controllers\Admin\Charts;

use App\Models\MerchantBank;
use App\Models\Transaction;
use Backpack\CRUD\app\Http\Controllers\ChartController;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;

/**
 * Class TransactionStatusChartController
 * @package App\Http\Controllers\Admin\Charts
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class TransactionStatusChartController extends ChartController
{
    public function setup()
    {
        $this->chart = new Chart();

        // MANDATORY. Set the labels for the dataset points
        $this->chart->labels([
            'Transaction Count',
        ]);

        // RECOMMENDED. Set URL that the ChartJS library should call, to get its data using AJAX.
        $this->chart->load(backpack_url('charts/transaction-status'));

        // OPTIONAL
        // $this->chart->minimalist(false);
        // $this->chart->displayLegend(true);
    }

    /**
     * Respond to AJAX calls with all the chart data points.
     *
     * @return json
     */
    public function data()
    {
        $transactionCount = Transaction::query()
            ->selectRaw('banks.code, sum(transactions.amount) as total')
            ->leftJoin('merchant_plans','merchant_plans.id','=','transactions.merchant_plan_id')
            ->leftJoin('merchant_banks','merchant_banks.id','=','merchant_plans.merchant_banks_id')
            ->leftJoin('banks','banks.id','=','merchant_banks.bank_id')
            ->where('transactions.status','=','PAID')
            ->groupBy('banks.code')
            ->get();

        dd($transactionCount);
        $this->chart->dataset('Paid', 'bar', [$transactionCount->paid])
            ->color('rgba(0, 214, 143, 1)')
            ->backgroundColor('rgba(0, 214, 143, 0.8)');

        $this->chart->dataset('Declined', 'bar', [$transactionCount->declined])
            ->color('rgba(205, 32, 31, 1)')
            ->backgroundColor('rgba(205, 32, 31, 0.8)');

        $this->chart->dataset('Pending', 'bar', [$transactionCount->pending])
            ->color('rgba(255, 204, 0, 1)')
            ->backgroundColor('rgba(255, 204, 0, 0.8)');
    }

    private function getBankTransactions()
    {
        $merchantBanks = MerchantBank::with('plans')->get();
        if (empty($merchantBanks)) {
            return null;
        }
        dd($merchantBanks);
        $merchantPlanIds = $merchantBanks->plans()->pluck('id')->toArray() ?? [];
        if (empty($merchantPlanIds)) {
            return null;
        }
        $transactions = Transaction::whereIn('merchant_plan_id', $merchantPlanIds)
            ->where('is_sent', 0)
            ->where('status', 'PAID')->get();
        if (empty($transactions) || $transactions->count() < 1) {
            return null;
        }
        return $transactions;
    }
}

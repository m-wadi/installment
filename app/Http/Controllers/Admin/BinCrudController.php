<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\BinRequest;
use App\Models\Plan;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class BinCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class BinCrudController extends CrudController
{
    use ListOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;
    use CreateOperation {
        store as traitStore;
    }

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Bin::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/bin');
        CRUD::setEntityNameStrings('bin', 'bins');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('bin');
        CRUD::column('bank_id');
        CRUD::column('created_at');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(BinRequest::class);

        $this->crud->addField([
            'name' => 'bank_id',
            'label' => 'Bank',
            'type' => "relationship",
        ]);

        $this->crud->addField(
            [   // Table
                'name' => 'bin',
                'label' => 'Enter Bins',
                'type' => 'table',
                'entity_singular' => 'bin', // used on the "Add X" button
                'columns' => [
                    'bin' => 'bin',
                ],
                'max' => 50, // maximum rows allowed in the table
                'min' => 1, // minimum rows allowed in the table
            ],
        );
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        CRUD::setValidation(BinRequest::class);
        CRUD::field('bin');
        $this->crud->addField([
            'name' => 'bank_id',
            'label' => 'Bank',
            'type' => "relationship",
        ]);
    }

    public function store()
    {
        $this->crud->hasAccessOrFail('create');

        // execute the FormRequest authorization and validation, if one is required
        $request = $this->crud->validateRequest();

        $bins = json_decode($this->crud->getStrippedSaveRequest()['bin'], true);
        $bankId = $this->crud->getStrippedSaveRequest()['bank_id'];

        // insert item in the db
        foreach ($bins as $bin) {
            $bin['bank_id'] = $bankId;
            $item = $this->crud->create($bin);
            $this->data['entry'] = $this->crud->entry = $item;
        }

        // show a success message
        \Alert::success(trans('backpack::crud.insert_success'))->flash();

        // save the redirect choice for next time
        $this->crud->setSaveAction();

        return $this->crud->performSaveAction($item->getKey());
    }
}

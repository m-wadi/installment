<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\BankRequest;
use App\Models\Bank;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class BankCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class BankCrudController extends CrudController
{
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(Bank::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/bank');
        CRUD::setEntityNameStrings('bank', 'banks');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('name');
        CRUD::column('email');
        CRUD::column('status');
        CRUD::column('country');
        CRUD::column('frequency');
        CRUD::column('code');
        CRUD::column('created_at');
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(BankRequest::class);

        CRUD::field('name');
        CRUD::field('code');
        $this->crud->addField([ // image
            'label' => "Bank Logo",
            'name' => "image",
            'type' => 'image',
            'upload' => true,
            'crop' => true, // set to true to allow cropping, false to disable
            'aspect_ratio' => 0, // ommit or set to 0 to allow any aspect ratio
        ]);
        $this->crud->addField([
            'name' => 'email',
            'label' => 'Emails',
            'type' => 'table',
            'hint' => 'User emails who the transaction file will be sent for.',
            'entity_singular' => 'email', // used on the "Add X" button
            'columns' => [
                'email' => 'Email',
            ],
            'max' => 6, // maximum rows allowed in the table
            'min' => 1, // minimum rows allowed in the table
        ]);
        $this->crud->addField([
            'name' => 'status',
            'type' => 'select_from_array',
            'options' => [
                '0' => 'Inactive',
                '1' => 'Active'
            ],
            'allows_null' => false,
            'default' => '1',
        ]);
        $this->crud->addField([
            'name' => 'frequency',
            'label' => 'Frequency',
            'hint' => 'Frequency of hours to send the file to the bank (24,48 ....)',
            'type' => 'number',
        ]);

        $this->crud->addField([
            'name' => 'phone',
            'label' => 'Phone',
            'type' => 'text'
        ]);

        CRUD::field('terms');


        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Enums\PaymentSetting;
use App\Http\Requests\PaymentSettingRequest;
use App\Models\Merchant;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class PaymentSettingCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PaymentSettingCrudController extends CrudController
{
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\PaymentSetting::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/payment-setting');
        CRUD::setEntityNameStrings('payment setting', 'payment settings');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('entity_id');
//        CRUD::column('access_token');
//        CRUD::column('environment');
        CRUD::column('currency');
        CRUD::column('connector');
        CRUD::column('payment_method');
//        CRUD::column('language');
        CRUD::column('merchant_id');
//        CRUD::column('deleted_at/');
        CRUD::column('created_at');

    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(PaymentSettingRequest::class);

        $this->crud->addFields([
            [
                // Select
                'label' => "Merchant",
                'type' => 'select',
                'name' => 'merchant_id', // the db column for the foreign key
                'entity' => 'merchant', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'model' => "App\Models\Merchant",
            ],
            [
                'name' => 'entity_id',
                'label' => "Entity Id",
            ],
            [
                'name' => 'access_token',
                'label' => "Access Token",
            ],
            [
                'name' => 'environment',
                'label' => "Environment",
                'type' => 'select2_from_array',
                'options' => ['' => 'Please select environment'] + PaymentSetting::ENVIRONMENTS
            ],
            [
                'name' => 'currency',
                'label' => "Currency",
                'type' => 'select2_from_array',
                'options' => ['' => 'Please select currency'] + PaymentSetting::CURRENCIES
            ],
            [
                'name' => 'connector',
                'label' => "Connector",
                'type' => 'select2_from_array',
                'options' => ['' => 'Please select connector'] + PaymentSetting::CONNECTORS
            ],
            [
                'name' => 'payment_method',
                'label' => "Payment Brand",
                'type' => 'select2_from_array',
                'allows_multiple' => true,
                'options' => ['' => 'Please select payment method'] + PaymentSetting::PAYMENT_METHODS
            ],
            [
                'name' => 'payment_type',
                'label' => "Payment Type",
                'type' => 'select2_from_array',
                'options' => ['' => 'Please select payment type'] + PaymentSetting::PAYMENT_TYPES
            ],
//            [
//                'name' => 'form_style',
//                'label' => "Form Style",
//                'type' => 'select2_from_array',
//                'options' => ['' => 'Please select form style'] + PaymentSetting::FORM_STYLES
//            ],
            [
                'name' => 'language',
                'label' => "Language",
                'type' => 'select2_from_array',
                'options' => ['' => 'Please select language'] + PaymentSetting::LANGUAGE_CODES
            ],
        ]);

    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}

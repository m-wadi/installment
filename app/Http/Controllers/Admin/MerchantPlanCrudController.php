<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\MerchantPlanRequest;
use App\Models\MerchantBank;
use App\Models\MerchantPlan;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;

/**
 * Class MerchantPlanCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class MerchantPlanCrudController extends CrudController
{
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(MerchantPlan::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/merchant-plan');
        CRUD::setEntityNameStrings('merchant plan', 'merchant plans');

        $this->crud->denyAccess(['update','show']);

        $this->crud->addButtonFromModelFunction('line', 'Add Bins', 'addMerchantBinsButton');
        $this->crud->addButtonFromModelFunction('line', 'List Bins', 'listMerchantBinsButton');

    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->removeButton('create');

        if (!empty(request()->get('merchantBankId'))) {
            $this->crud->addClause('where', 'merchant_banks_id', '=', request()->get('merchantBankId'));
        }

        $this->crud->addColumn([
            'name' => 'merchant_id',
            'label' => 'Merchant',
            'type' => 'closure',
            'function' => function ($entry) {
                return $entry->merchantBank->merchant->name;
            }
        ]);

        $this->crud->addColumn([
            'name' => 'banks_id',
            'label' => 'Bank',
            'type' => 'closure',
            'function' => function ($entry) {
                return $entry->merchantBank->bank->name;
            }
        ]);

        $this->crud->addColumn([
            'name' => 'plan_id',
            'label' => 'Plan',
            'type' => "relationship",
        ]);
        CRUD::column('created_at');

    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        if (request()->isMethod('post')){
            $merchantBankId = request()->post('merchant_banks_id');
        }else{
            $merchantBankId = request()->get('merchantBankId');
        }

        if (empty($merchantBankId)) {
            abort(400, 'No merchant bank selected');
        }
        $merchantBank = MerchantBank::where('id', $merchantBankId)->with('bank', 'merchant')->first();
        if (empty($merchantBank)) {
            abort(404, 'Merchant bank not found');
        }

        CRUD::setValidation(MerchantPlanRequest::class);

        $this->crud->addField(
            [
                'name' => 'name', // JSON variable name
                'label' => "Merchant Bank", // human-readable label for the input
                'fake' => true, // show the field, but don't store it in the database column above,
                'default' => $merchantBank->merchant->name . ' / ' . $merchantBank->bank->name,
                'attributes' => [
                    'readonly' => 'readonly',
                    'disabled' => 'disabled',
                ],
            ]
        );

        $this->crud->addField([
            'name' => 'merchant_banks_id',
            'type' => 'hidden',
            'value' => $merchantBankId,
        ]);

        $this->crud->addField([
            'name' => 'plan_id',
            'label' => 'Plan',
            'type' => 'select_multiple',
            'entity' => 'plan', // the method that defines the relationship in your Model
            'model' => "App\Models\Plan", // foreign key model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'allows_null' => false,
            'options' => (function ($query) use ($merchantBank) {
                return $query->where('bank_id', $merchantBank->bank->id)->get();
            }),
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        abort(403);
        $this->setupCreateOperation();
    }

    public function store()
    {
        $this->crud->hasAccessOrFail('create');

        // execute the FormRequest authorization and validation, if one is required
        $request = $this->crud->validateRequest();
        $plans = $this->crud->getStrippedSaveRequest()['plan_id'] ?? null;
        $merchantBankId = $this->crud->getStrippedSaveRequest()['merchant_banks_id'] ?? null;
        if (empty($merchantBankId)){
            return \redirect()->back()->withErrors('No merchant bank selected');
        }
        if (empty($plans)){
            return \redirect()->back()->withErrors('No plans selected');
        }

        // insert item in the db
        foreach ($plans as $plan) {
            $newMerchantPlan['plan_id'] = $plan;
            $newMerchantPlan['merchant_banks_id'] = $merchantBankId;

            if (MerchantPlan::where('merchant_banks_id',$merchantBankId)->where('plan_id',$plan)->exists()){
                if (end($plans) == $plan){
                    return redirect()->back()->withErrors('Plan Already assigned to this merchant');
                }
                \Alert::error('Plan Already assigned to this merchant')->flash();
                continue;
            }

            $item = $this->crud->create($newMerchantPlan);
            $this->data['entry'] = $this->crud->entry = $item;
        }

        // show a success message
        \Alert::success(trans('backpack::crud.insert_success'))->flash();

        // save the redirect choice for next time
        $this->crud->setSaveAction();

        return $this->crud->performSaveAction($item->getKey());
    }
}

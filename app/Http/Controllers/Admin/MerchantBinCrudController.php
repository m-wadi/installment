<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\MerchantBinRequest;
use App\Models\MerchantBin;
use App\Models\MerchantPlan;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class MerchantBinCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class MerchantBinCrudController extends CrudController
{
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(MerchantBin::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/merchant-bin');
        CRUD::setEntityNameStrings('merchant bin', 'merchant bins');

        $this->crud->denyAccess(['update','show']);
        $this->crud->removeButton('create');

    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->removeButton('create');

        if (!empty(request()->get('merchantPlanId'))) {
            $this->crud->addClause('where', 'merchant_plans_id', '=', request()->get('merchantPlanId'));
        }

        $this->crud->addColumn([
            'name' => 'merchant_id',
            'label' => 'Merchant',
            'type' => 'closure',
            'function' => function ($entry) {
                return $entry->merchantPlan->merchantBank->merchant->name;
            }
        ]);

        $this->crud->addColumn([
            'name' => 'banks_id',
            'label' => 'Bank',
            'type' => 'closure',
            'function' => function ($entry) {
                return $entry->merchantPlan->merchantBank->bank->name;
            }
        ]);

        $this->crud->addColumn([
            'name' => 'plans_id',
            'label' => 'Plan',
            'type' => 'closure',
            'function' => function ($entry) {
                return $entry->merchantPlan->plan->name;
            }
        ]);

        $this->crud->addColumn([
            'name' => 'bin_id',
            'label' => 'Bin',
            'type' => "relationship",
        ]);
        CRUD::column('created_at');
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        if (request()->isMethod('post')){
            $merchantPlanId = request()->post('merchant_plans_id');
        }else{
            $merchantPlanId = request()->get('merchantPlanId');
        }

        if (empty($merchantPlanId)) {
            abort(400, 'No merchant bank selected');
        }
        $merchantPlan = MerchantPlan::where('id', $merchantPlanId)->with('plan', 'merchantBank')->first();
        if (empty($merchantPlan)) {
            abort(404, 'Merchant plan not found');
        }

        CRUD::setValidation(MerchantBinRequest::class);

        $this->crud->addField(
            [
                'name' => 'name', // JSON variable name
                'label' => "Merchant Plan", // human-readable label for the input
                'fake' => true, // show the field, but don't store it in the database column above,
                'default' => $merchantPlan->merchantBank->merchant->name . ' / '
                    . $merchantPlan->merchantBank->bank->name . ' / ' . $merchantPlan->plan->name,
                'attributes' => [
                    'readonly' => 'readonly',
                    'disabled' => 'disabled',
                ],
            ]
        );

        $this->crud->addField([
            'name' => 'merchant_plans_id',
            'type' => 'hidden',
            'value' => $merchantPlanId,
        ]);

        $this->crud->addField([
            'name' => 'bin_id',
            'label' => 'Bin',
            'type' => 'select_multiple',
            'entity' => 'bin', // the method that defines the relationship in your Model
            'model' => "App\Models\Bin", // foreign key model
            'attribute' => 'bin', // foreign key attribute that is shown to user
            'allows_null' => false,
            'options' => (function ($query) use ($merchantPlan) {
                return $query->where('bank_id', $merchantPlan->merchantBank->bank->id)->get();
            }),
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        abort(403);
        $this->setupCreateOperation();
    }

    public function store()
    {
        $this->crud->hasAccessOrFail('create');

        // execute the FormRequest authorization and validation, if one is required
        $request = $this->crud->validateRequest();
        $bins = $this->crud->getStrippedSaveRequest()['bin_id'] ?? null;
        $merchantPlanId = $this->crud->getStrippedSaveRequest()['merchant_plans_id'] ?? null;
        if (empty($merchantPlanId)){
            return \redirect()->back()->withErrors('No merchant plan selected');
        }
        if (empty($bins)){
            return \redirect()->back()->withErrors('No bins selected');
        }

        // insert item in the db
        foreach ($bins as $bin) {
            $newMerchantPlan['bin_id'] = $bin;
            $newMerchantPlan['merchant_plans_id'] = $merchantPlanId;

            if (MerchantBin::where('merchant_plans_id',$merchantPlanId)->where('bin_id',$bin)->exists()){
                if (end($bins) == $bin){
                    return redirect()->back()->withErrors('Bin Already assigned to this plan/merchant');
                }
                \Alert::error('Bin Already assigned to this plan/merchant')->flash();
                continue;
            }

            $item = $this->crud->create($newMerchantPlan);
            $this->data['entry'] = $this->crud->entry = $item;
        }

        // show a success message
        \Alert::success(trans('backpack::crud.insert_success'))->flash();

        // save the redirect choice for next time
        $this->crud->setSaveAction();

        return $this->crud->performSaveAction($item->getKey());
    }
}

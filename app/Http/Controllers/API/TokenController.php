<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\GenerateTokenRequest;
use App\Models\Merchant;
use App\Models\SessionToken;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class TokenController extends Controller
{
    public function generateToken(GenerateTokenRequest $request)
    {
        try {
            // check if merchant exists
            $merchant = Merchant::where('secret', $request->accountKey)->with([
                'paymentSetting' => function ($q) use ($request) {
                    $q->where('entity_id', $request->entityId);
                }
            ])->first();
            if (empty($merchant)) { // TODO : change msg
                return response()->json(['errors' => 'There was an error in your request.'], Response::HTTP_NOT_FOUND);
            }

            if (empty($merchant->paymentSetting[0])) {
                return response()->json(['errors' => 'There was an error in your request'], Response::HTTP_BAD_REQUEST);
            }
            // check if merchant is active and his ip is whitelisted
            $whitelisted_ips = (array_column(json_decode($merchant->whitelist_ip, true), 'ip'));
            if ($merchant->status != 1 || !in_array($request->getClientIp(), $whitelisted_ips)) {
                return response()->json(['errors' => 'Unauthorized access'], Response::HTTP_UNAUTHORIZED);
            }

            // check if the merchantTransactionId is duplicate
            if (Transaction::where('merchant_transaction_id', $request->merchantTransactionId)
                ->where('merchant_id', $merchant->id)->exists()) {
                return response()->json(['errors' => 'Duplicate transaction for merchant'], Response::HTTP_BAD_REQUEST);
            }

            $transaction = Transaction::storeTransaction([
                'currency' => $request->currency,
                'amount' => $request->amount,
                'merchantTransactionId' => $request->merchantTransactionId,
                'merchantId' => $merchant->id,
                'shopperResultUrl' => $request->shopperResultUrl,
                'paymentSettingId' => $merchant->paymentSetting[0]->id
            ]);
            if (empty($transaction)) {
                return response()->json(['errors' => 'There was an error in transaction, please try again'], Response::HTTP_BAD_REQUEST);
            }

            $token = SessionToken::generate($transaction);
            if (empty($token)) {
                return response()->json(['errors' => 'There was an error in transaction, please try again'], Response::HTTP_BAD_REQUEST);
            }

            return response()->json([
                'session_id' => $token->token,
                'created_at' => $token->created_at,
                'expires_at' => $token->expires_at
            ], 200);
        } catch (\Exception $e) {
            return response()->json(['errors' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}

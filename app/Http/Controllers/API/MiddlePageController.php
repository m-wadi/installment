<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\PaymentFormRequest;
use App\Models\MerchantBank;
use App\Models\MerchantPlan;
use App\Services\CardService;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

class MiddlePageController extends Controller
{
    public function getMerchantPlans()
    {
        try {
            $merchantBankId = request()->get('merchantBankId');
            $merchantId = request()->get('merchantId');
            if (empty(request()->get('merchantBankId'))) {
                return response()->json('Error choosing you bank.', Response::HTTP_BAD_REQUEST);
            }
            $plans = MerchantPlan::where('merchant_banks_id', $merchantBankId)
                ->whereHas('merchantBank.merchant', function ($q) use ($merchantId) {
                    $q->where('id', $merchantId);
                })
                ->with('plan')
                ->with('merchantBank.merchant')
                ->get()->toArray();

            if (empty($plans)) {
                return response()->json('No available plans for select bank.', Response::HTTP_NOT_FOUND);
            }

            $bankName = MerchantBank::where('id', $merchantBankId)->with('bank')->first();

            $data['bankName'] = $bankName->bank->name;
            $data['plans'] = $plans;

            return response()->json($data);
        } catch (\Exception $e) {
            Log::error($e);
            return response()->json('Unknown error occurred', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function validatePaymentFormRequest(PaymentFormRequest $request)
    {
        try {
            $cardService = new CardService();
            // checking card number
            if (!$cardService->validateCardNumber($request->cardnumber)) {
                return response()->json('Incorrect card number', Response::HTTP_BAD_REQUEST);
            }
            // checking card expiry
            if (!$cardService->validateExpiryDate($request->expirationdate)) {
                return response()->json('Card is expired', Response::HTTP_BAD_REQUEST);
            }
            // check card CVV
            if (!$cardService->validateCVV($request->securitycode)) {
                return response()->json('Wrong card CVV', Response::HTTP_BAD_REQUEST);
            }
            // checking card brand
            if (!$cardService->detectedCardBrand($request->cardnumber)) {
                return response()->json('Incorrect card brand', Response::HTTP_BAD_REQUEST);
            }

            return response()->json('success');
        } catch (\Exception $e) {
            Log::error($e);
            return response()->json('Unknown error occurred', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }
}

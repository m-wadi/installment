<?php

namespace App\Http\Controllers;

use App\Http\Requests\MiddlePageRequest;
use App\Jobs\TransactionInquiryAndUpdate;
use App\Models\SessionToken;
use App\Services\CardService;
use App\Services\PaymentService;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;

class MiddlePageController extends Controller
{
    public function showMiddlePage($token)
    {
        try {
            $sessionToken = SessionToken::where('token', $token)
                ->with('transaction.merchant.banks.bank')
                ->first();

            if (empty($sessionToken)) {
                return view('middlePage.error')->with('error', 'Session token does not exists.');
            }

            if ($sessionToken->is_used) {
                return view('middlePage.error')->with('error', 'Session token already used.');
            }

            if ($sessionToken->expires_at < now()) {
                return view('middlePage.error')->with('error', 'Session token is expired.');
            }

            if (empty($sessionToken->transaction)) {
                return view('middlePage.error')->with('error', 'Transaction does not exists');
            }
            if (request()->has('lang')){
                \app()->setLocale(request()->get('lang'));
            }else{
                \app()->setLocale('ar');
            }
            $merchantName = $sessionToken->transaction->merchant->name;
            return view('middlePage.' . $merchantName . '.payment')
                ->with('token', $token)
                ->with('merchant', $sessionToken->transaction->merchant);
        } catch (\Exception $e) {
            Log::error($e);
            return view('middlePage.error')->with('error', 'Unexpected error occurred');
        }
    }

    public function paymentRequest(MiddlePageRequest $request)
    {
        try {
            // check if validation failed
            if ($request->validator->fails()) {
                $errors = array_values($request->validator->errors()->getMessages());
                $errors = array_map(function ($a) {
                    return array_pop($a);
                }, $errors);
                $imploded = implode('<br>', $errors);
                return view('middlePage.error')->with('error', $imploded);
            }
            $cardService = new CardService();
            // checking card number
            if (!$cardService->validateCardNumber($request->cardnumber)) {
                return view('middlePage.error')->with('error', 'Incorrect card number');
            }
            // checking card expiry
            if (!$cardService->validateExpiryDate($request->expirationdate)) {
                return view('middlePage.error')->with('error', 'Card is expired');
            }
            // check card CVV
            if (!$cardService->validateCVV($request->securitycode)) {
                return view('middlePage.error')->with('error', 'Wrong card CVV');
            }
            // checking card brand
            if (!$cardService->detectedCardBrand($request->cardnumber)) {
                return view('middlePage.error')->with('error', 'Incorrect card brand');
            }

            // get card bin
            $bin = $cardService->getCardBin($request->cardnumber);
            if (!$bin) {
                return view('middlePage.error')->with('error', 'Incorrect card brand');
            }

            $sessionToken = SessionToken::where('token', $request->paymentSessionToken)
                ->with(['transaction.merchant.banks' => function ($merchantBankQuery) use ($bin, $request) {
                    $merchantBankQuery->where('id', $request->merchantBankId);
                    $merchantBankQuery->with(['plans' => function ($merchantPlanQuery) use ($bin, $request) {
                        $merchantPlanQuery->where('id', $request->merchantPlanId);
                    }]);
                }])
                ->first();

            if (empty($sessionToken)) {
                return view('middlePage.error')->with('error', 'Session token does not exists.');
            }

            if ($sessionToken->is_used) {
                return view('middlePage.error')->with('error', 'Session token already used.');
            }

            if ($sessionToken->expires_at < now()) {
                return view('middlePage.error')->with('error', 'Session token is expired.');
            }

            if ($sessionToken->transaction->merchant_id != $request->merchantId) {
                return view('middlePage.error')->with('error', 'Session token does not belong to merchant');
            }

            if (empty($sessionToken->transaction->merchant->banks)) {
                return view('middlePage.error')->with('error', 'Bank not belong to merchant')
                    ->with('token',$sessionToken->token);
            }

            if (empty($sessionToken->transaction->merchant->banks->first())) {
                return view('middlePage.error')->with('error', 'Bank not belong to merchant')
                    ->with('token',$sessionToken->token);
            }

            $plan = $sessionToken->transaction->merchant->banks->first()->plans->first();
            if (empty($plan)) {
                return view('middlePage.error')->with('error', 'Plan not belong to merchant')
                    ->with('token',$sessionToken->token);
            }
            //check if card bin belongs to the selected plan
            if (!$this->checkBinPlan($bin, $plan)) {
                return view('middlePage.error')->with('error', 'Your card is not permitted for this bank/plan')
                    ->with('token',$sessionToken->token);
            }

            // check if the transaction amount is within bank rules
            if ($sessionToken->transaction->amount < $plan->plan->min_value) {
                return view('middlePage.error')->with('error', 'The minimum value for the selected plan is ' . $plan->plan->min_value)
                    ->with('token',$sessionToken->token);
            }
            $sessionToken->update(['is_used' => 1]);
            $sessionToken->transaction->update(['merchant_plan_id' => $request->merchantPlanId]);

            $cardData = $cardService->prepareCardData($request->cardnumber, $request->expirationdate, $request->name, $request->securitycode);

            $threeDsecure = [
                'ip' => $request->ip(),
                'acceptHeaders' => $request->header()['accept'][0],
                'colorDepth' => $request->colorDepth ?? '',
                'language' => $request->language ?? '',
                'height' => $request->height ?? '',
                'width' => $request->width ?? '',
                'timeZone' => $request->timeZone ?? '',
                'userAgent' => $request->userAgent ?? '',
                'challengeWindow' => $request->challengeWindow ?? '',
                'javaEnabled' => true,
            ];

            $paymentService = new PaymentService($sessionToken, $cardData ,$threeDsecure);
            $paymentService->preparePaymentData();
            $response = $paymentService->performPaymentRequest();

            $sessionToken->transaction->update(['result_details' => json_encode($response)]);

            if ($response->result->code == '000.200.000') {
                $sessionToken->transaction->update(['card' => $cardService->stripWhiteSpacesInCard($request->cardnumber)]);
                $sessionToken->transaction->update(['unique_id' => $response->id]);
                TransactionInquiryAndUpdate::dispatch($sessionToken)->delay(now()->addMinutes(30));
                echo $paymentService->buildRedirectForm($response);
            } else {
                return view('middlePage.error')->with('token',$sessionToken->token)
                    ->with('error', 'An error occurred in your payment please contact the merchant : session: ' . $sessionToken->token);
            }

            return view('middlePage.error')->with('error', 'Unknown error occurred , code : 90167');
        } catch (\Exception $e) {
            Log::error($e);
            return view('middlePage.error')->with('error', 'Unknown error occurred code : 90170');
        }
    }

    private function checkBinPlan($bin, $plan)
    {
        return in_array($bin, $plan->bins->pluck('bin.bin')->toArray());
    }

}

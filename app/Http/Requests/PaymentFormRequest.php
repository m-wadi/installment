<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Symfony\Component\HttpFoundation\Response;

class PaymentFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'expirationdate' => 'required',
//            'merchantBankId' => 'required',
//            'merchantId' => 'required',
//            'merchantPlanId' => 'required',
            'securitycode' => 'required',
            'cardnumber' => 'required',
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Card holder name is required',
            'expirationdate.required' => 'Card expiry date is required',
//            'merchantBankId' => 'Missing data (Bank)',
//            'merchantId' => 'Missing data (Merchant)',
//            'merchantPlanId' => 'Missing data (Plan)',
            'securitycode.required' => 'Card CVV is required',
            'cardnumber.required' => 'Card number is required',
        ];
    }

    public function failedValidation(Validator $validator)
    {
        $errors = array_values($validator->errors()->getMessages());
        $errors = array_map(function($a) {  return array_pop($a); }, $errors);

        $imploded = implode('<br>', $errors);
        throw new HttpResponseException(
            response()->json($imploded, Response::HTTP_BAD_REQUEST)
        );
    }
}

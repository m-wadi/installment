<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Symfony\Component\HttpFoundation\Response;

class GenerateTokenRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => 'required|regex:/^\d{1,10}+(\.\d{2})?$/',
            'currency' => 'required|in:SAR,JOD',
            'shopperResultUrl' => 'required|url',
            'entityId' => 'required',
            'merchantTransactionId' => 'required',
            'accountKey' => 'required',
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            response()->json(['errors' => array_values($validator->errors()->messages())], Response::HTTP_BAD_REQUEST)
        );
    }

}

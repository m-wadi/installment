<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class MiddlePageRequest extends FormRequest
{
    public $validator = null;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'expirationdate' => 'required',
            'merchantBankId' => 'required',
            'merchantId' => 'required',
            'merchantPlanId' => 'required',
            'securitycode' => 'required',
            'cardnumber' => 'required|min:15|max:19',
            'paymentSessionToken' => 'required'
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Card holder name is required',
            'expirationdate.required' => 'Card expiry date is required',
            'merchantBankId.required' => 'Missing data (Bank)',
            'merchantId.required' => 'Missing data (Merchant)',
            'merchantPlanId.required' => 'Missing data (Plan)',
            'securitycode.required' => 'Card CVV is required',
            'cardnumber.required' => 'Card number is required',
            'paymentSessionToken.required' => 'Payment session not exists'
        ];
    }

    public function failedValidation(Validator $validator)
    {
        $this->validator = $validator;
    }
}

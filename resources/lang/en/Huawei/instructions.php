<?php

return [
    'instructionsTitle' => 'Installment Terms',

    'instructionsBody' => '
The following provisions apply to any transactions made using Installments as a payment option on https://consumer.huawei.com/sa/ (hereinafter referred to as "Huawei.sa") Huawei allows Installments ("Installments") payment method on all purchases on Huawei.sa using eligible credit cards.
Your issuing bank facilitates the Installment payment method. If your bank rejects your request, your order on Huawei.sa will not be converted into an Installment payment plan and your Credit Card will be charged for the total amount due. Installments is not available on purchases made using Debit cards or Cash on Delivery payment methods.
Installments is offered by the relevant bank directly to the customer; Huawei.sa has no role to play in the approval, extension, pricing, modification, pre-closure, closure or any matter incidental thereto pertaining to offering of the Installment, which is decided at the sole discretion of the relevant bank. The Installment being offered by the bank to the customers is governed by the respective terms and conditions of each bank/issuer and the customer is advised to approach the bank/issuer in case of any questions, complaint, dispute or enquiry about an Installment transaction.
Huawei.sa will use reasonable efforts to display Installment related information (Installment amount, interest rate charged, total amount payable) for the customer\'s purchase on Huawei.sa as per the information shared with it by the relevant bank on an "AS IS" basis. Banks/issuers are the authoritative source of this information and customers are advised to directly contact their bank/issuer for any further clarifications in this regard. For more information, review the terms and conditions of the relevant bank offering such Installment facility.

After choosing a suitable installments plan and making the payment please directly contact your bank for installment application. '
];

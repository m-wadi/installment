<?php

return [
    'merchant' => 'Huawei',
    'title' => 'Installments',
    'chooseBank' => 'Please choose your bank:',
    'instructions' => 'Read the instructions',
    'banksPhone' => 'For more info:',
    'banksTermsText' => 'Please read:',
    'banksTermsLink' => 'Terms & Conditions',
    'paymentDivTitle' => 'Payment Information',
    'proceedPayment' => 'Proceed',
    'cardHolder' => 'Card Holder Name',
    'cardNumber' => 'Card Number',
    'cardExpiration' => 'Expiration (mm/yy)',
    'cardCVV' => 'Security Code',
    'instructionsButton' => 'Instructions'

];

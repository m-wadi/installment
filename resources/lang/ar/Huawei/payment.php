<?php

return [
    'merchant' => 'هواوي',
    'title' => 'اقساط',
    'chooseBank' => 'الرجاء اختيار البنك:',
    'instructions' => 'التعليمات',
    'banksPhone' => 'لمزيد من المعلومات:',
    'banksTermsText' => 'يرجى قراءة:',
    'banksTermsLink' => 'الشروط والأحكام',
    'paymentDivTitle' => 'معلومات الدفع',
    'proceedPayment' => 'ادفع',
    'cardHolder' => 'اسم حامل البطاقة',
    'cardNumber' => 'رقم البطاقة',
    'cardExpiration' => 'تاريخ انتهاء البطاقة',
    'cardCVV' => 'رمز الأمان',
    'instructionsButton' => 'التعليمات'
];

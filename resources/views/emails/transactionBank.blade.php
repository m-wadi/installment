<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <style>
        * {
            font-family: Arial, Helvetica, sans-serif;
        }

        .container {
            justify-content: center;
            margin: 0 auto;
            text-align: center;
            width: 500px;
            border: 1px solid #00a65a;
            border-radius: 20px;
            overflow: hidden;
        }

        .message {
            text-align: start;
            padding-left: 20px;
            white-space: pre-line;

        }

        .logo {
            padding: 20px;
            margin-bottom: 20px;
            border-bottom: 1px solid #00a65a;
        }

        .footer {
            margin-top: 30px;
            padding-top: 30px;
            background: #00a65a;
            color: white;
            display: block;
            width: 100%;
            height: 70px;
        }
    </style>
</head>

<body>
<div class="container">
    <div class="logo">
        <img src="{{asset('images/hyperpay.png')}}">
    </div>

    <div class="message">
        Please be informed that
        the instalment file <b>{{$fileName}} </b>
        is attached
    </div>

    <div class="footer">
        Hyperpay Installments
    </div>

</div>
</body>

</html>

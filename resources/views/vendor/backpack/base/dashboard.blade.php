@extends(backpack_view('blank'))
@section('content')
    @php
        use App\Http\Controllers\Admin\Charts as Charts;


    $widgets['content'][] = [
        'type'       => 'chart',
        'controller' => Charts\TransactionBanksChartController::class,
        'content' => [
            'header' => 'Bank Transactions',
        ],
        'wrapper'       => ['class' => 'col-sm-6 col-md-6'],
    ];

    $widgets['before_content'][] = [
        'type'=> 'div',
        'class' => 'row',
        'content'     =>  $widgets['content'],
    ];
    @endphp


{{--    $widgets['content'][] = [--}}
{{--    'type'       => 'chart',--}}
{{--    'controller' => Charts\TransactionStatusChartController::class,--}}
{{--    'content' => [--}}
{{--    'header' => 'Transactions count',--}}
{{--    ],--}}
{{--    'wrapperClass' => ['class', 'row'],--}}
{{--    'wrapper'       => ['class' => 'col-sm-6 col-md-6'],--}}

{{--    ];--}}
@endsection

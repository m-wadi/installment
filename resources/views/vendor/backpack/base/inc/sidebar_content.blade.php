<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item">
    <a class="nav-link" href="{{ backpack_url('dashboard') }}">
        <i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}
    </a>
</li>

<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-group"></i> Authentication</a>
    <ul class="nav-dropdown-items">
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('user') }}"><i
                    class="nav-icon la la-user"></i> <span>Users</span></a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('role') }}"><i
                    class="nav-icon la la-group"></i> <span>Roles</span></a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('permission') }}"><i
                    class="nav-icon la la-key"></i> <span>Permissions</span></a></li>
    </ul>
</li>

<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-group"></i> Merchant Configration</a>
    <ul class="nav-dropdown-items">
        <li class="nav-item">
            <a class="nav-link" href="{{ backpack_url('merchant') }}">
                <i class="nav-icon la la-user"></i> <span>Merchants</span></a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{ backpack_url('merchant-bank') }}">
                <i class="nav-icon la la-bank"></i> <span>Merchants Bank</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ backpack_url('merchant-plan') }}">
                <i class="nav-icon la la-list-alt"></i> <span>Merchants Plan</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ backpack_url('merchant-bin') }}">
                <i class="nav-icon la la-credit-card"></i> <span>Merchants Bin</span></a>
        </li>
    </ul>
</li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('payment-setting') }}'>
        <i class='nav-icon la la-money'></i> Payment settings
    </a>
</li>

<li class='nav-item'>
    <a class='nav-link' href='{{ backpack_url('bank') }}'><i class='nav-icon la la-bank'></i>
        Banks
    </a>
</li>

<li class='nav-item'>
    <a class='nav-link' href='{{ backpack_url('plan') }}'><i class='nav-icon la la-list-alt'></i>
        Plans
    </a>
</li>

<li class='nav-item'>
    <a class='nav-link' href='{{ backpack_url('bin') }}'><i class='nav-icon la la-credit-card'></i>
        Bins
    </a>
</li>

<li class='nav-item'>
    <a class='nav-link' href='{{ backpack_url('transaction') }}'><i class='nav-icon la la-money'></i>
        Transactions
    </a>
</li>

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">

<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <title>Hyperpay Installments - Error</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('css/error.css')}}">
</head>

<body>
<div class="container">

    <div class="jumbotron jumbotron-fluid ">
        <div class="container">
            <h4>
                <div><img height='110' src="{{asset('images/hyperpay.png')}}"></div>
                <hr>
                {{--                <span style="color: #dc3545!important">Error : {{ $error ?? 'Sorry, Something went wrong.' }}</span>--}}
                <span style="color: #dc3545!important">Error : {!! $error ?? 'Sorry, Something went wrong.' !!}</span>

                @if(isset($token))
                    <hr>
                    <a href="{{url('plans/redirect/' . $token)}}"> Go back </a>
                @endif
            </h4>
        </div>

    </div>
</div>
</body>

</html>
